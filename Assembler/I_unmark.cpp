/*
* I_unmark.cpp
* Gisi Musa
*/

#include "allHeaders.h"

I_unmark::I_unmark() { }
I_unmark::~I_unmark() { }

void I_unmark::read() { 
    
    return "unmark " + to_string(unmark) + " " + to_string(line+1) + "\n";

}


int I_unmark::parse(string, args, int line){

     // regex pattern
    regex unmarkRegex("[\t]*unmark ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, unmarkRegex)) {
        // regex was successfully matched
        
        int unmarkValue = stoi(match.str(1));
        
        if (unmarkValue < 0 || unmarkValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            unmark = unmarkValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid arguments");
        return -1;
    }
    this->line = line;
    return line+1;
    
    
}