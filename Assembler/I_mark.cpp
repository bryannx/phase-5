/*
* l_mark.cpp
* Gisi Musa
*/

#include "allHeaders.h"
#include <string>
#include <iostream>
#include <vector>
#include <regex>

using namespace std;


I_mark::I_mark(Labels *list) : Instruction(list) {}


/**
 * @brief extracts necessary information from the given string
 * 
 * @param args string to be parsed
 * @param line line number
 * @return int next line number
 */

int I_mark::parse(string args, int line) {
    // regex pattern
    regex markRegex("[\t]*mark ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, markRegex)) {
        // regex was successfully matched
        
        int markValue = stoi(match.str(1));
        
        if (markValue < 0 || markValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            mark = markValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid argument");
        return -1;
    }
    this->line = line;
    return line+1;
}

string I_mark::read() {
    return "mark " + to_string(mark) + " " + to_string(line+1) + "\n";

}
