/*
* I_mark.h
* Gisi Musa
*/

#ifndef I_MARK_h
#define I_MARK_h
#include "allHeaders.h"

using namespace std;

class I_mark: public Instruction
{
    public:
        I_mark();
        int parse(string args, int line);
        string read();
        virtual ~I_mark();

    private:
        tmark mark;
};

#endif // I_MARK_h