/*
* I_sense.cpp
* Gisi Musa
*/

#include "allHeaders.h"
#include <sstream>

I_sense::I_sense() { 

}

I_sense::~I_sense() {

}

void I_sense::read() { 
    
    return "sense " + to_string(sense) + " " + to_string(line+1) + "\n";

}


int I_sense::parse(string, args, int line){

     // regex pattern
    regex senseRegex("[\t]*sense ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, senseRegex)) {
        // regex was successfully matched
        
        int senseValue = stoi(match.str(1));
        
        if (senseValue < 0 || senseValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            sense = senseValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid arguments");
        return -1;
    }
    this->line = line;
    return line+1;
    
    
}