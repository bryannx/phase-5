/*
 * Labels.cpp
 * Brian Ngugi
 */

#include "Labels.h"
#include "allHeaders.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

Labels::Labels() {
    this->labels = map<string, int>();
    this->gotos = map<int, string>();
}

int Labels::line_of(string label) {
    
    cout << label << " " << labels[label] << endl;
    return labels[label];
}

void Labels::add_label(string label, int line) {
    cout << "Label " << label << " at " << line << endl;
    labels[label] = line;
}

void Labels::add_goto(string label, int line) {
    gotos[line] = label;
}

int Labels::resolve_goto(int line) {
    return 0;
}



