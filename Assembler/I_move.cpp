/*
* I_move.cpp
* Gisi Musa
*/

#include "allHeaders.h"

I_move::I_move() { }
I_move::~I_move() { }


void I_move::read() { 
    
    return "move " + to_string(move) + " " + to_string(line+1) + "\n";

}


int I_move::parse(string, args, int line){

     // regex pattern
    regex moveRegex("[\t]*move ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, moveRegex)) {
        // regex was successfully matched
        
        int moveValue = stoi(match.str(1));
        
        if (moveValue < 0 || moveValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            move = moveValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid arguments");
        return -1;
    }
    this->line = line;
    return line+1;
    
    
}