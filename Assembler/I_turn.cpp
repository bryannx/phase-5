/*
* l_turn.cpp
* Gisi Musa
*/

#include "allHeaders.h"

l_turn::l_turn() { }
l_turn::~l_turn() { }

void I_turn::read() { 
    
    return "turn " + to_string(turn) + " " + to_string(line+1) + "\n";

}


int I_turn::parse(string, args, int line){

     // regex pattern
    regex turnRegex("[\t]*turn ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, turnRegex)) {
        // regex was successfully matched
        
        int turnValue = stoi(match.str(1));
        
        if (turnValue < 0 || turnValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            turn = turnValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid arguments");
        return -1;
    }
    this->line = line;
    return line+1;
    
    
}