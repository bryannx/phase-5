/*
* l_pickup.cpp
* Gisi Musa
*/

#include "allHeaders.h"

l_pickup::l_pickup() { }
l_pickup::~l_pickup() { }

void I_pickup::read() { 
    
    return "pickup " + to_string(pickup) + " " + to_string(line+1) + "\n";

}


int I_pickup::parse(string, args, int line){

     // regex pattern
    regex pickupRegex("[\t]*pickup ([+-]?[1-9]\\d*|0)[\t]*");
    
    // contains the matches
    smatch match;

    if (regex_search(args, match, pickupRegex)) {
        // regex was successfully matched
        
        int pickupValue = stoi(match.str(1));
        
        if (pickupValue < 0 || pickupValue > 5) {
            //Value between 0 and 5
            Instruction::error("Line "+ to_string(line) +": invalid argument: \""+ match.str(1) +"\"");
            return -1;
        } else {
            // value between 0 and 5 
            pickup = pickupValue;
        }
    } else {
        // regex did not match and therefore must be to litle arguments
        Instruction::error("Line "+ to_string(line) +": not enough or invalid arguments");
        return -1;
    }
    this->line = line;
    return line+1;
    
    
}